import store from "./store/store.mjs"

import {CheckValue, ChangeValue} from "./otherFile"

store.todo.mutations.addTodo('hello', 1)
CheckValue()
store.todo.mutations.addTodo('abbc', 2)
CheckValue()
store.todo.mutations.addTodo('123', 3)
CheckValue()
store.todo.mutations.removeTodo(1)

CheckValue()
ChangeValue()
CheckValue()
console.log(`store.hero.getters.name`, store.hero.getters.name)
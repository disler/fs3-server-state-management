class TodoState {
	constructor() {
		this.todos = []
	}
}

const todoState = new TodoState()

const mutations = {
	addTodo(note, id) {
		todoState.todos.push({note, id})
	},
	removeTodo(id) {
		const index = todoState.todos.findIndex(_todo => _todo.id === id)
		todoState.todos.splice(index, 1)
	},
}

const getters = {
	get todos() {
		return todoState.todos
	}
}

export default {mutations, getters}
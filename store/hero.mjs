class HeroState {
	constructor() {
		this.name = []
	}
}

const heroState = new HeroState()

const mutations = {
	setName(name) {
		heroState.name = name
	},
}

const getters = {
	get name() {
		return heroState.name
	}
}

export default {mutations, getters}
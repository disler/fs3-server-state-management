import todo from "./todo"
import hero from "./hero"

const imports = {todo, hero}

const store = {}

const proxyHandler = {
	get(target, property, receiver) {
		return function() {
			console.log(`target`, target)
			console.log(`property`, property)
			console.log(`arguments`, arguments)
			
			target[property].apply(this, arguments)
		}
	}
}

Object.keys(imports).forEach(_key => {
	const _import = imports[_key]
	_import.mutations = new Proxy(_import.mutations, proxyHandler)
	store[_key] = _import
})

export default store